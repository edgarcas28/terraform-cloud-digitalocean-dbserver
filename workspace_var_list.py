import pyterprise
import sys
import time

if(len(sys.argv) < 2):
    print(" ")
    print("python3 workspace_var_list.py <workspace>")
    print(" ")
    sys.exit(1)
    
with open ("/Users/ej/.ssh/terraform1.pem","r") as myfile:
    tfe_token = myfile.read()
client = pyterprise.Client()

# Supply your token as a parameter and the url for the terraform enterprise server.
# If you are not self hosting, use the one provided by hashicorp.
client.init(token=tfe_token, url='https://app.terraform.io')



# Set the organization
org = client.set_organization(id="erich-ej-best-fl")
workspace = org.get_workspace(sys.argv[1])

output = {}
# Print all variables for a workspace. 
# Variables objects also have functions for updating and deleting.
for variable in workspace.list_variables():
    output[variable.key] = variable.value

output_new = {key: value for key, value in sorted(output.items())}
for key, value in output_new.items():
    print(key)