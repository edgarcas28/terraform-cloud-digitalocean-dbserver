# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 17:13:15 2022

@author: RP
"""

from selenium import webdriver
from selenium.webdriver.support import ui
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.chrome.options import Options
from selenium import webdriver   # for webdriver
from webdriver_manager.firefox import GeckoDriverManager
driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
import time
import sys

def main(wrkspace_name):

    driver.get('https://app.terraform.io/session')
    
    username = 'erich.ej.best.fl@gmail.com'
    password = '!Baseball12345678'
    
    #Username Enter
    usrnme = driver.find_element(By.ID, "user_login")
    usrnme.send_keys(username)
    
    #Password Enter
    psswd = driver.find_element(By.ID, "user_password")
    psswd.send_keys(password)
    
    #Click on Submit Button
    driver.find_element(By.XPATH, '/html/body/div/div/div/div/section/div[2]/div/form/div[3]/input').click()
    time.sleep(15)
    #Click on erich-ej-best-fl
    workspace_name = driver.find_element(By.ID, 'ember30').text
    driver.find_element(By.ID, 'ember30').click()
    #driver.find_element(By.CLASS_NAME, 'has-text-weight-bold').click()
    
    
    
    #Select Workspace Name
    time.sleep(2)
    workspace = wrkspace_name
    #workspace = 'xyz-123-pdq-test-workspace'
    driver.get('https://app.terraform.io/app/'+ workspace_name + '/workspaces/' + workspace + '/settings/delete')
    
    time.sleep(10)
    driver.find_element(By.XPATH, '/html/body/div[1]/div/div/div[4]/div/div/section/form[2]/div/div/button').click()
    
    #Enter Workspace Name
    time.sleep(4)
    psswd = driver.find_element(By.ID, "input-confirm")
    psswd.send_keys(workspace)
    time.sleep(2)
    driver.find_element(By.XPATH, '/html/body/div[1]/div/div/div[7]/div/div[3]/footer/button[1]').click()
    
    time.sleep(60)
    driver.find_element(By.XPATH, '/html/body/div[1]/div/div/div[4]/div/div/div/div[3]/div[2]/div/div/div/button[1]').click()
    
    time.sleep(5)
    driver.find_element(By.XPATH, '/html/body/div[1]/div/div/div[4]/div/div/div/div[3]/div[2]/div/div/form/div[2]/div[2]/div/button[1]').click()

if __name__ == "__main__":
    wrkspace_name = sys.argv[1]
    main(wrkspace_name)
    