FROM mysql/mysql-server:latest
ENV MYSQL_ROOT_HOST: '%'
ARG DB_ROOT_PW=default_pass
ENV MYSQL_ROOT_PASSWORD=${DB_ROOT_PW}
ENV MYSQL_DATABASE=ejs
ENV MYSQL_USER=dbuser_sa
ARG DB_PW=default_pass
ENV MYSQL_PASSWORD=${DB_PW}

COPY ./mysql_certs/* /mysql_certs/
RUN chown -R mysql:mysql /mysql_certs/

COPY my.cnf /etc/

ADD ejs.sql /docker-entrypoint-initdb.d/
ADD privileges.sql /docker-entrypoint-initdb.d/

